import sys 

# from django.shortcuts import render
from django.conf import settings
from django.contrib.auth.models import User
from django.utils import timezone

from rest_framework import viewsets
from rest_framework.decorators import renderer_classes, detail_route, list_route
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer

from .serializers import UserSerializer, SedutaSerializer, PuntoOdGSerializer, PostazioneSerializer, PrenotazioneSerializer
from .models import Seduta, PuntoOdG, PuntoOdGTrattato, Postazione, Prenotazione, Intervento

from ws4redis.publisher import RedisPublisher
from ws4redis.redis_store import RedisMessage

from pymodbus.client.sync import ModbusTcpClient as ModbusClient


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class SedutaViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Seduta.objects.all()
    serializer_class = SedutaSerializer

    @detail_route(methods=['get'])
    @renderer_classes((JSONRenderer,))
    def termina(self, request, pk=None):
        seduta = self.get_object()

        PuntoOdGTrattato.objects.filter(punto__seduta=seduta).exclude(started=None).filter(finished=None).update(finished=timezone.now())

        if seduta.started is None or seduta.finished is not None:
            return Response({
                'done': False
            })

        seduta.finished=timezone.now()
        seduta.save()

        return Response({
            'done': True
        })


class PuntoViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = PuntoOdG.objects.all()
    serializer_class = PuntoOdGSerializer

    @detail_route(methods=['get'])
    @renderer_classes((JSONRenderer,))
    def tratta(self, request, pk=None):
        punto = self.get_object()

        seduta_in_corso = Seduta.in_corso()
        if not seduta_in_corso or seduta_in_corso.pk != punto.seduta.pk:
            if seduta_in_corso:
                seduta_in_corso.finished=timezone.now()
                seduta_in_corso.save()

            punto.seduta.started=timezone.now()
            punto.seduta.finished=None
            punto.seduta.save()
  
        PuntoOdGTrattato.objects.filter(punto__seduta=punto.seduta).exclude(started=None).filter(finished=None).update(finished=timezone.now())
        trattato = PuntoOdGTrattato.objects.create(punto=punto, started=timezone.now())

        return Response({
            'done': True
        })


class PostazioneViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Postazione.objects.all()
    serializer_class = PostazioneSerializer

    def __init__(self, *args, **kwargs):
        super(PostazioneViewSet, self).__init__(*args, **kwargs)
        self.web_redis_publisher = RedisPublisher(facility='web', broadcast=True)
        if settings.ARDUINOWS:
            self.arduino_redis_publisher = RedisPublisher(facility='arduino', broadcast=True)
        else:
            self.arduino_redis_publisher = None

    @list_route(methods=['get'])
    @renderer_classes((JSONRenderer,))
    def prenota(self, request):
        seduta = Seduta.in_corso()
        if seduta is None:
            return Response({
                'done': False
            })
        # print >>sys.stderr, 'seduta:', seduta

        if 'source' not in request.query_params or 'channel' not in request.query_params or 'status' not in request.query_params:
            return Response({
                'done': False
            })

        if request.query_params['status'] != '0':
            return Response({
                'done': True
            })

        postazioni = self.queryset.filter(modbus_addr=request.query_params['source'], modbus_coil=request.query_params['channel'])
        if postazioni.count()==0:
            return Response({
                'done': False
            })

        for postazione in postazioni:
            if seduta.prenotazioni_da_evadere().filter(postazione=postazione).count() == 0:
                prenotazione = Prenotazione.objects.create(seduta=seduta, postazione=postazione, requested=timezone.now())
                self.web_redis_publisher.publish_message(RedisMessage('+'))
                print >>sys.stderr, 'prenotazione:', prenotazione
            else:
                print >>sys.stderr, 'postazione ALREADY:', postazione

        return Response({
            'done': True
        })

    @detail_route(methods=['get'])
    @renderer_classes((JSONRenderer,))
    def intervento(self, request, pk=None):
        postazione = self.get_object()

        arduinoLVL = 99 if postazione.level == 100 else postazione.level

        if postazione.on_air():
            postazione.interventi.filter(finished=None).update(finished=timezone.now())
            arduinoLVL = 0
        else:
            intervento = Intervento(postazione=postazione, seduta_id=request.query_params['seduta'], started=timezone.now())
            intervento.save()

        if self.arduino_redis_publisher is not None:
            arduinoCMD = '{}{}'.format(postazione.arduino, arduinoLVL)
            self.arduino_redis_publisher.publish_message(RedisMessage(arduinoCMD))

        if postazione.modbus_addr != '' and postazione.modbus_coil is not None:
            try:
                with ModbusClient(postazione.modbus_addr) as client:
                  client.write_coil(postazione.modbus_coil, (True, False)[arduinoLVL == 0])
                  client.close()
            except:
                pass

        return Response({
            'done': True
        })

    @detail_route(methods=['get'])
    @renderer_classes((JSONRenderer,))
    def level(self, request, pk=None):
        postazione = self.get_object()
        postazione.level = request.query_params['level']
        postazione.save()

        return Response({
            'done': True
        })


class PrenotazioneViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Prenotazione.objects.filter(fulfilled=None, canceled=None)
    serializer_class = PrenotazioneSerializer

    def __init__(self, *args, **kwargs):
        super(PrenotazioneViewSet, self).__init__(*args, **kwargs)
        if settings.ARDUINOWS:
            self.arduino_redis_publisher = RedisPublisher(facility='arduino', broadcast=True)
        else:
            self.arduino_redis_publisher = None

    @detail_route(methods=['get'])
    @renderer_classes((JSONRenderer,))
    def fulfill(self, request, pk=None):
        prenotazione = self.get_object()
        prenotazione.fulfilled = timezone.now()
        prenotazione.save()

        prenotazione.seduta.interventi.filter(finished=None).update(finished=prenotazione.fulfilled)
        intervento = Intervento(postazione=prenotazione.postazione, seduta=prenotazione.seduta, prenotazione=prenotazione, started=prenotazione.fulfilled)
        intervento.save()

        postazione = prenotazione.postazione

        if postazione.modbus_addr != '' and postazione.modbus_coil is not None:
            try:
                with ModbusClient(postazione.modbus_addr) as client:
                  client.write_coil(postazione.modbus_coil, (True, False)[arduinoLVL == 0])
                  client.close()
            except:
                pass

        if self.arduino_redis_publisher is not None:
            arduinoLVL = 99 if postazione.level == 100 else postazione.level
            arduinoCMD = '{}{}'.format(postazione.arduino, arduinoLVL)
            self.arduino_redis_publisher.publish_message(RedisMessage(arduinoCMD))

        return Response({
            'done': True
        })
