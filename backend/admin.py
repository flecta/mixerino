from django.contrib import admin

from models import *  # NOQA

# Register your models here.


admin.site.register(Postazione)
admin.site.register(Seduta)
admin.site.register(PuntoOdG)
admin.site.register(Prenotazione)


class PuntoOdGTrattatoAdmin(admin.ModelAdmin):
    list_display = ('started', 'punto', 'finished')
admin.site.register(PuntoOdGTrattato, PuntoOdGTrattatoAdmin)

class InterventoAdmin(admin.ModelAdmin):
    list_display = ('started', 'postazione', 'finished')
admin.site.register(Intervento, InterventoAdmin)
