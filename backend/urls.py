from django.conf.urls import url, include

from rest_framework import routers

from .views import UserViewSet, SedutaViewSet, PuntoViewSet, PostazioneViewSet, PrenotazioneViewSet


router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'sedute', SedutaViewSet)
router.register(r'punti', PuntoViewSet)
router.register(r'postazioni', PostazioneViewSet)
router.register(r'prenotazioni', PrenotazioneViewSet)


urlpatterns = [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/', include(router.urls)),
]
