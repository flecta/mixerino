from rest_framework import serializers

from django.contrib.auth.models import User

from .models import Seduta, PuntoOdG, Postazione, Prenotazione


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')


class SedutaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Seduta
        fields = ('pk', 'label')


class PuntoOdGSerializer(serializers.ModelSerializer):
    class Meta:
        model = PuntoOdG
        fields = ('pk', 'seduta', 'titolo')


class PostazioneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Postazione
        fields = ('pk', 'label', 'level')


class PrenotazioneSerializer(serializers.ModelSerializer):
    postazione = PostazioneSerializer(many=False, read_only=True)

    class Meta:
        model = Prenotazione
        fields = ('pk', 'requested', 'postazione')
