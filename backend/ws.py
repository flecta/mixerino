# -*- coding: utf-8 -*-
import sys

from django.utils import timezone

from .models import Postazione, Seduta, Prenotazione

from ws4redis.subscriber import RedisSubscriber
from ws4redis.publisher import RedisPublisher
from ws4redis.redis_store import RedisMessage


class ArduinoSubscriber(RedisSubscriber):
  def __init__(self, *args, **kwargs):
    super(ArduinoSubscriber, self).__init__(*args, **kwargs)
    self.redis_publisher = RedisPublisher(facility='web', broadcast=True)

  def parse_response(self):
    r = super(ArduinoSubscriber, self).parse_response()
    # print >>sys.stderr, r
    if r[0] == 'message' and r[1] == 'ws:broadcast:arduino':
      msg = r[2]
      print >>sys.stderr, 'arduino msg:', msg
      if msg[0] == 'P':
        try:
          postazione = Postazione.objects.get(arduino=msg[1])
          seduta = Seduta.in_corso()
          if seduta is not None:
            # print >>sys.stderr, 'seduta:', seduta
            if seduta.prenotazioni_da_evadere().filter(postazione=postazione).count() == 0:
              prenotazione = Prenotazione(seduta=seduta, postazione=postazione, requested=timezone.now())
              # print >>sys.stderr, 'prenotazione:', prenotazione
              prenotazione.save()
              self.redis_publisher.publish_message(RedisMessage('+'))
            else:
              print >>sys.stderr, 'postazione', postazione, 'già prenotata'
          else:
            print >>sys.stderr, 'nessuna seduta in corso'
        except Postazione.DoesNotExist:
          print >>sys.stderr, 'nessuna postazione identificata da', msg[1]
    return r
