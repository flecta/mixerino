from __future__ import unicode_literals

from django.conf import settings
from django.db import models


class Postazione(models.Model):
    label = models.CharField(max_length=100, blank=True, default='')
    level = models.IntegerField(default=0)
    arduino = models.CharField(max_length=1, blank=True, default='')
    modbus_addr = models.CharField(max_length=15, blank=True, default='')
    modbus_coil = models.IntegerField(null=True, blank=True, default=None)

    class Meta:
        ordering = ('pk',)
        verbose_name_plural = "postazioni"

    def on_air(self):
        return self.interventi.filter(finished=None).count() > 0

    def btn_class(self):
        return ('default', 'success')[self.on_air()]

    def __unicode__(self):
        if settings.ARDUINOWS:
            return u'{} ({}%)'.format(self.label, self.level)
        else:
            return self.label


class Seduta(models.Model):
    label = models.CharField(max_length=200, blank=True, default='')

    started = models.DateTimeField(null=True, blank=True, db_index=True)
    finished = models.DateTimeField(null=True, blank=True, db_index=True)

    # punto01 = models.TextField(blank=True, default='')
    # punto02 = models.TextField(blank=True, default='')
    # punto03 = models.TextField(blank=True, default='')
    # punto04 = models.TextField(blank=True, default='')
    # punto05 = models.TextField(blank=True, default='')
    # punto06 = models.TextField(blank=True, default='')
    # punto07 = models.TextField(blank=True, default='')
    # punto08 = models.TextField(blank=True, default='')
    # punto09 = models.TextField(blank=True, default='')
    # punto10 = models.TextField(blank=True, default='')

    # punto11 = models.TextField(blank=True, default='')
    # punto12 = models.TextField(blank=True, default='')
    # punto13 = models.TextField(blank=True, default='')
    # punto14 = models.TextField(blank=True, default='')
    # punto15 = models.TextField(blank=True, default='')
    # punto16 = models.TextField(blank=True, default='')
    # punto17 = models.TextField(blank=True, default='')
    # punto18 = models.TextField(blank=True, default='')
    # punto19 = models.TextField(blank=True, default='')
    # punto20 = models.TextField(blank=True, default='')

    class Meta:
        ordering = ('-started',)
        verbose_name_plural = "sedute"

    def prenotazioni_da_evadere(self):
        return self.prenotazioni.filter(fulfilled=None, canceled=None)

    def __unicode__(self):
        return self.label if self.label else str(self.pk)

    @staticmethod
    def in_corso():
        return Seduta.objects.exclude(started=None).filter(finished=None).first()


class PuntoOdG(models.Model):
    seduta = models.ForeignKey(Seduta, models.CASCADE, related_name='punti')
    punto = models.IntegerField(db_index=True)
    titolo = models.CharField(max_length=200, blank=True, default='')
    note = models.TextField(blank=True, default='')

    def in_corso(self):
        return self.trattati.filter(finished=None).last()

    class Meta:
        ordering = ('seduta', 'punto',)
        verbose_name = "punto o.d.g."
        verbose_name_plural = "punti o.d.g."

    def __unicode__(self):
        return u'{}: {}. {}'.format(self.seduta, str(self.punto), self.titolo)


class PuntoOdGTrattato(models.Model):
    punto = models.ForeignKey(PuntoOdG, models.SET_NULL, null=True, blank=True, related_name='trattati')
    started = models.DateTimeField(null=True, blank=True, db_index=True)
    finished = models.DateTimeField(null=True, blank=True, db_index=True)

    class Meta:
        ordering = ('-started',)
        verbose_name = "punto o.d.g. trattato"
        verbose_name_plural = "punti o.d.g. trattati"


class Prenotazione(models.Model):
    seduta = models.ForeignKey(Seduta, models.CASCADE, related_name='prenotazioni')
    postazione = models.ForeignKey(Postazione, models.SET_NULL, null=True, blank=True)
    requested = models.DateTimeField(null=True, blank=True, db_index=True)
    fulfilled = models.DateTimeField(null=True, blank=True, db_index=True)
    canceled = models.DateTimeField(null=True, blank=True, db_index=True)

    class Meta:
        ordering = ('seduta', 'requested',)
        verbose_name_plural = "prenotazioni"

    def __unicode__(self):
        return u'{}: {}'.format(self.requested, self.postazione)


class Intervento(models.Model):
    prenotazione = models.ForeignKey(Prenotazione, models.SET_NULL, related_name='interventi', null=True, blank=True)
    seduta = models.ForeignKey(Seduta, models.CASCADE, related_name='interventi')
    postazione = models.ForeignKey(Postazione, models.SET_NULL, related_name='interventi', null=True, blank=True)
    started = models.DateTimeField(null=True, blank=True, db_index=True)
    finished = models.DateTimeField(null=True, blank=True)

    class Meta:
        ordering = ('-started',)
        verbose_name_plural = "interventi"

    def __unicode__(self):
        return u'{} -> {}: {}'.format(self.started.time, self.finished, self.postazione)
