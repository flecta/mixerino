
from pysnmp.hlapi import *

id = '1.3.6.1.4.1.8691.10.1214'

nt = NotificationType(ObjectIdentity(id), (), {})
# nt.addVarBinds(ObjectType(ObjectIdentity(id, '1', 0)))
# nt.addVarBinds(ObjectType(ObjectIdentity(id), '20.1'))

g = sendNotification(SnmpEngine(),
                     CommunityData('public'),
                     UdpTransportTarget(('127.0.0.1', 1162)),
                     ContextData(),
                     'trap',
                     nt
                     )
print next(g)
