#!/usr/bin/python
# -*- coding: utf-8 -*-
import threading
import time
import sys
import traceback

import requests

from pysnmp.carrier.asynsock.dispatch import AsynsockDispatcher
from pysnmp.carrier.asynsock.dgram import udp
# from pysnmp.carrier.asynsock.dgram import udp6
from pyasn1.codec.ber import decoder
from pysnmp.proto import api

from pymodbus.client.sync import ModbusTcpClient


API_SERVER = '192.168.48.126:8000'


class MoxaSnmpServer(threading.Thread):
	def __init__(self, debug=False):
		super(MoxaSnmpServer,self).__init__()
		self.setDaemon(1)

		self.relayBoards = {
			# '10.0.9.99':{ 0:0, 1:0, 2:0, 3:0, 4:0, 5:0, },
			'192.168.48.121':{ 0:0, 1:0, 2:0, 3:0, 4:0, 5:0, },
			'192.168.48.122':{ 0:0, 1:0, 2:0, 3:0, 4:0, 5:0, },
			'192.168.48.123':{ 0:0, 1:0, 2:0, 3:0, 4:0, 5:0, },
			'192.168.48.124':{ 0:0, 1:0, 2:0, 3:0, 4:0, 5:0, },
		}

		self.moxaMsgID='1.3.6.1.4.1.8691.10.1214'
		self.moxaMsgInputsID=self.moxaMsgID+'.20.'
		self.moxaMsgRelaysID=self.moxaMsgID+'.21.'

		self.transportDispatcher = AsynsockDispatcher()
		self.transportDispatcher.registerRecvCbFun(self.trapcall)
		self.transportDispatcher.registerTransport(udp.domainName, udp.UdpSocketTransport().openServerMode(('0.0.0.0', 162)))
		#self.transportDispatcher.registerTransport(udp6.domainName, udp6.Udp6SocketTransport().openServerMode(('::1', 162)))
		
		self.debugDI=debug
		self.debugDO=debug

	def run(self):
		self.transportDispatcher.jobStarted(1)
		try:
			# Dispatcher will never finish as job#1 never reaches zero
			self.transportDispatcher.runDispatcher()
		except:
			self.transportDispatcher.closeDispatcher()
			raise

	def trapcall(self, transportDispatcher, transportDomain, transportAddress, wholeMsg):
		if wholeMsg:
			try:
				msgVer = int(api.decodeMessageVersion(wholeMsg))
				if msgVer not in api.protoModules:
					print('Unsupported SNMP version {}'.format(msgVer))
					return

				# self.relaySet(source='10.0.9.99',channel=5,status=0)
				# return

				pMod = api.protoModules[msgVer]
				reqMsg, wholeMsg=decoder.decode(wholeMsg, asn1Spec=pMod.Message())
				reqPDU = pMod.apiMessage.getPDU(reqMsg)

				if not reqPDU.isSameTypeWith(pMod.TrapPDU()):
					print('not same type')
					return

				if pMod.apiTrapPDU.getEnterprise(reqPDU).prettyPrint() != self.moxaMsgID:
					print('not moxa msg')
					return					

				if msgVer == api.protoVersion1:
					agentAddress=pMod.apiTrapPDU.getAgentAddr(reqPDU).prettyPrint()
					#specificTrap=pMod.apiTrapPDU.getSpecificTrap(reqPDU).prettyPrint()
					#uptime=pMod.apiTrapPDU.getTimeStamp(reqPDU).prettyPrint()
					varBinds = pMod.apiTrapPDU.getVarBindList(reqPDU)
				else:
					varBinds = pMod.apiPDU.getVarBindList(reqPDU)

				for oid, val in varBinds:
					oid=str(oid)
					if oid.startswith(self.moxaMsgInputsID):
						channel=oid.replace(self.moxaMsgInputsID,'')
						channel=int(channel)-1
						status=int(val.getComponent().getComponent().prettyPrint())
						if self.debugDI: print('[%s] DI%d = %d' % (agentAddress, channel, status))
						self.relaySet(source=agentAddress,channel=channel,status=status)

					elif oid.startswith(self.moxaMsgRelaysID):
						channel=oid.replace(self.moxaMsgRelaysID,'')
						channel=int(channel)-1
						status=int(val.getComponent().getComponent().prettyPrint())
						if self.debugDO: print('[%s] DO%d = %d' % (agentAddress, channel, status))
						#self.world.relayBoardStatusUpdate(agentAddress,channel,status)
				else:
					print "no varBinds"
			except:
				print('Notification message ERROR from %s:%s: '%(transportDomain, transportAddress))
				exc_type, exc_value, exc_traceback = sys.exc_info()
				# traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
				traceback.print_exception(exc_type, exc_value, exc_traceback, limit=2, file=sys.stdout)
		return wholeMsg

	def relaySet(self, source, channel, status):
		reply = requests.get('http://{}/api/postazioni/prenota/?source={}&channel={}&status={}'.format(API_SERVER, source, channel, status)).json()
		if reply['done'] == True:
			self.relayBoards[source][channel] = status

	def relaysCheck(self):
		for boardAddress in self.relayBoards:
			#print '[relaysCheck] boardAddress:',boardAddress
			try:
				client = ModbusTcpClient(boardAddress)
				for channel in self.relayBoards[boardAddress]:
					#print '[relaysCheck]   local.ch%d=%d'%(channel,self.relayBoards[boardAddress][channel])
					if channel<16:
						try:
							result = client.read_coils(channel,1)
							if result!=None:
								result_value=(0,1)[result.bits[0]]
								#print '[relaysCheck]   remote.ch%d=%d'%(channel,result_value)
								if result_value!=self.relayBoards[boardAddress][channel]:
									client.write_coil(channel,(False,True)[self.relayBoards[boardAddress][channel]==1])
									errTitle='RELAY STATUS FIXED!'
									errMsg='fixed relay %d of device %s: set to %d'%(channel,boardAddress,self.relayBoards[boardAddress][channel])
									if self.debugDO: print errTitle, errMsg
						except KeyboardInterrupt:
							raise KeyboardInterrupt
						except:
							print 'ERROR in read_coils/relaysCheck'
							exc_type, exc_value, exc_traceback = sys.exc_info()
							#traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
							traceback.print_exception(exc_type, exc_value, exc_traceback, limit=2, file=sys.stdout)
					#else:
					#	print 'skipping when modbus channel is above 16, since moxa devices use that channel for PULSE inputs...'
			except KeyboardInterrupt:
				raise KeyboardInterrupt
			except:
				print 'ERROR in ModbusTcpClient communication with',boardAddress
				exc_type, exc_value, exc_traceback = sys.exc_info()
				#traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
				traceback.print_exception(exc_type, exc_value, exc_traceback, limit=2, file=sys.stdout)

if __name__ == "__main__":
	trapserver=MoxaSnmpServer(debug=True)
	trapserver.start()

	go_on = True
	while go_on:
		try:
			# trapserver.relaysCheck()
			time.sleep(3)
		except KeyboardInterrupt:
			go_on = False
		except:
			pass

	print "exited!"
