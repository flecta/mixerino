import serial
import traceback
import sys
import time
from serial.threaded import *  # NOQA
# from serial.tools import list_ports

import websocket
import thread


class ArduinoReader(LineReader):
  feedback = []

  def connection_made(self, transport):
    super(ArduinoReader, self).connection_made(transport)
    sys.stdout.write('port opened\n')
    # self.write_line('hello world')

  def data_received(self, data):
    """Buffer received data, find TERMINATOR, call handle_packet"""
    self.buffer.extend(data)
    while self.TERMINATOR in self.buffer:
      packet, self.buffer = self.buffer.split(self.TERMINATOR, 1)
      self.handle_packet(packet)

  def handle_line(self, data):
    try:
      if data[0] in ['L', 'P']:
        self.feedback.append(data)
      else:
        sys.stdout.write('line received: {}\n'.format(repr(data)))
    except:
      pass

  def connection_lost(self, exc):
    if exc:
      traceback.print_exc(exc)
    sys.stdout.write('port closed\n')
    self.feedback = []


class Mixerino():
    @staticmethod
    def on_message(ws, message):
      if message != 'reset!':
        print 'to arduino:', message
        ws.arduino.write_line(message)

    @staticmethod
    def on_error(ws, error):
      print error

    @staticmethod
    def on_close(ws):
      print "### webservice closed ###"

    @staticmethod
    def on_open(ws):
      print "### webservice opened ###"

      def run(*args):
        while ws.keep_running:
          feedback_ok = True
          while feedback_ok and len(ws.arduino.feedback) > 0:
            message = ws.arduino.feedback.pop(0)
            print 'from arduino:', message
            try:
              ws.send(message)
            except:
              print 'could not deliver: putting msg back into queue'
              ws.arduino.feedback.insert(0, message)
              feedback_ok = False
          time.sleep(1)
        print "thread terminating..."
      thread.start_new_thread(run, ())


if __name__ == "__main__":
  ser = serial.serial_for_url('hwgrep://TTL232R', baudrate=9600, timeout=1)
  with ReaderThread(ser, ArduinoReader) as arduino:
    while True:
      # websocket.enableTrace(True)
      ws = websocket.WebSocketApp("ws://127.0.0.1:8000/ws/arduino?subscribe-broadcast&publish-broadcast",
                                  on_open=Mixerino.on_open,
                                  on_message=Mixerino.on_message,
                                  on_error=Mixerino.on_error,
                                  on_close=Mixerino.on_close)
      ws.arduino = arduino
      ws.run_forever()
      print 'will try to reconnect in a few seconds...'
      time.sleep(5)
