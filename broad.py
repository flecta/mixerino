#!/usr/bin/env python
import os

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mixerino.settings")

    from ws4redis.publisher import RedisPublisher
    from ws4redis.redis_store import RedisMessage

    redis_publisher = RedisPublisher(facility='foobar', broadcast=True)
    message = RedisMessage('A0')
    # and somewhere else
    redis_publisher.publish_message(message)
