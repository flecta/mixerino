#include <Arduino.h>

/*
  Mixerino

  This controls an Analog Devices AD5206 digital potentiometer.
  The AD5206 has 6 potentiometer channels. Each channel's pins are labeled
  A - connect this to voltage
  W - this is the pot's wiper, which changes when you set it
  B - connect this to ground.

 The AD5206 is SPI-compatible,and to command it, you send two bytes,
 one with the channel number (0 - 5) and one with the resistance value for the
 channel (0 - 255).

 The circuit:
  * All A pins of AD5206 connected audio inputs
  * All B pins of AD5206 connected to ground
  * Audio outputs connected to each W pin
  * CS - to digital pin 10  (SS pin)
  * SDI - to digital pin 11 (MOSI pin)
  * CLK - to digital pin 13 (SCK pin)

 created 2016 by Enrico Marchesin
*/


/*
MUST MODIFY SOFTWARESERIAL LIBRARY TO COMPILE
/Applications/Arduino 1.6.1/Arduino.app/Contents/Resources/Java/hardware/arduino/avr/libraries/SoftwareSerial/SoftwareSerial.cpp
*/

// COMMENT LINES: 225-244
/*
#if defined(PCINT0_vect)
ISR(PCINT0_vect)
{
  SoftwareSerial::handle_interrupt();
}
#endif

#if defined(PCINT1_vect)
ISR(PCINT1_vect, ISR_ALIASOF(PCINT0_vect));
#endif

#if defined(PCINT2_vect)
ISR(PCINT2_vect, ISR_ALIASOF(PCINT0_vect));
#endif

#if defined(PCINT3_vect)
ISR(PCINT3_vect, ISR_ALIASOF(PCINT0_vect));
#endif
*/
// AND ADD THE FOLLOWING:
/*
ISR(PCINT1_vect)
{
  SoftwareSerial::handle_interrupt();
}
*/




// include the SPI library:
#include <SPI.h>
// and the software library:
#include <SoftwareSerial.h>

SoftwareSerial cascade(14, 15); // RX, TX

boolean DEBUG = false;

// set pin 10 as the slave select for the digital pot:M
const int slaveSelectPin = 10, ledPin = 13;
int levels[] = {0, 0, 0, 0, 0, 0};
int currentIndex=-1, numRead1=-1, numRead2=-1, cmd=-1, cmd2=-1, butRead1=-1, butRead2=-1;
byte byteRead, let;

long debounceDelay = 100;
long lastChangeTime[] = {0, 0, 0, 0, 0, 0};
int lastState[] = {0, 0, 0, 0, 0, 0};

void setup() {
  pinMode (ledPin, OUTPUT);

  Serial.begin(9600);
  Serial.println("reset!");

  cascade.begin(9600);

  // set the slaveSelectPin as an output:
  pinMode (slaveSelectPin, OUTPUT);
  // initialize SPI:
  SPI.begin();
  updateLevels();

  InitialiseIO();
  InitialiseInterrupt();

  pulseLed();
}

void InitialiseIO(){
  // PORTB, PCINT0
  pinMode(9, INPUT);
  pinMode(8, INPUT);
  // PORTD, PCINT2
  pinMode(7, INPUT);
  pinMode(6, INPUT);
  pinMode(5, INPUT);
  pinMode(4, INPUT);
}

void InitialiseInterrupt(){
  cli();		// switch interrupts off while messing with their settings  
  PCICR = 0x05;         // Enable PCINT1 & PCINT0 interrupt
  PCMSK0 = 0b00000011;
  PCMSK2 = 0b11110000;
  sei();		// turn interrupts back on
}

void digitalReadHighDebounced(int pin, int index) {
  int reading = digitalRead(pin);
  if (lastState[index]==0 && reading==1) {
    lastChangeTime[index] = millis();
  } else if ( lastState[index]==1 && reading==0 && (millis()-lastChangeTime[index])>debounceDelay ) {
      Serial.print("P");
      Serial.write( (65+index) );
      Serial.println("=1");
      pulseLed();
  }
  lastState[index] = reading;
}

ISR(PCINT0_vect) {
  digitalReadHighDebounced(9,0);
  digitalReadHighDebounced(8,1);
}

ISR(PCINT2_vect) {
  digitalReadHighDebounced(7,2);
  digitalReadHighDebounced(6,3);
  digitalReadHighDebounced(5,4);
  digitalReadHighDebounced(4,5);
}

void pulseLed() {
  //digitalWrite(ledPin, 1);
  //delay(100);
  //digitalWrite(ledPin, 0);
}

// fixes modfied input order on pcb design
int cmd2pcb(int idx) {
  int r=idx;
  if (idx==0) {
    r=3;
  } else if (idx==2) { 
    r=0;
  } else if (idx==3) {
    r=2;
  }
  return r;
}

byte idx2let(int idx) {
  if (idx<18) {
    return (byte)(65+idx);
  } else {
    return (byte)(97+(idx-18));
  }
}

void loop() {
  while (Serial.available()) {
    byteRead = Serial.read();
    if (cmd==-1 && byteRead==76) {
      // ON CHAR '?', RETURN ALL LINE LEVELS
    } else if (cmd==-1 && byteRead==63) {
      // ON CHAR '?', RETURN ALL LINE LEVELS
      updateLevels();
    } else if (cmd==-1 && byteRead==33) {
      // ON CHAR '!', SILENCE ALL LINES
      for (int inputLine = 0; inputLine < 6; inputLine++) {
        levels[inputLine] = 0;
      }
      updateLevels();
    } else if (cmd==-1 && byteRead>64 && byteRead<89) {
      cmd=0;
      // ON CHARS [A-X], SET CHANNEL INDEX (first half)
      currentIndex = byteRead - 65;
      if (DEBUG) {
        Serial.print("input=");
        Serial.print( currentIndex );
        Serial.println("");
      }
    } else if (cmd==-1 && byteRead>96 && byteRead<121) {
      // ON CHARS [a-x], SET CHANNEL INDEX (second half)
      currentIndex = 18 + (byteRead - 97);
      if (DEBUG) {
        Serial.print("input=");
        Serial.print( currentIndex );
        Serial.println("");
      }
    } else if (cmd==0 && byteRead>47 && byteRead<58) {
      // ON NUMBERS [0-9], SET LEVEL
      if (numRead1==-1) {
        numRead1 = byteRead - 48;
      } else if (numRead2==-1) {
        numRead2 = byteRead - 48;
        if (DEBUG) {
          Serial.print("level=");
          Serial.print(numRead1);
          Serial.print(numRead2);
          Serial.println("");
        }
      }
    } else if (cmd!=-1 && (byteRead==10 || byteRead==13)) {
      if (cmd==0 && currentIndex!=-1 && numRead1!=-1) {
        if (numRead2==-1) {
          numRead2=numRead1;
          numRead1=0;
        }
        if (currentIndex>5) {
          let = idx2let(currentIndex - 6);
          if (DEBUG) {
            Serial.println("cascading as:");
            Serial.write(let);
            Serial.print(numRead1);
            Serial.print(numRead2);
            Serial.println("");
          }
          cascade.write(let);
          cascade.print(numRead1);
          cascade.print(numRead2);
          cascade.println("");
        } else {
          levels[currentIndex] = ( (numRead1*10) + numRead2 ) * 3;
          if (levels[currentIndex]>255) levels[currentIndex]=255;
          if (DEBUG) {
            Serial.print("input");
            Serial.print(currentIndex);
            Serial.print("=");
            Serial.print(levels[currentIndex]);
            Serial.println("");
          }
          updateLevel(currentIndex);
        }
        pulseLed();
      }
      currentIndex = -1;
      numRead1 = -1;
      numRead2 = -1;
      cmd = -1;
    }
  }

  while (cascade.available()) {
    byteRead = cascade.read();
    if (cmd2==-1 && byteRead==80) {
      // ON CHAR 'P', START PARSING CHANNEL NUM
      cmd2=1;
    } else if (cmd2==1 && byteRead>47 && byteRead<58) {
      // ON NUMBERS [0-9], SET CHANNEL
      if (butRead1==-1) {
        butRead1 = byteRead - 48;
      } else if (butRead2==-1) {
        butRead2 = byteRead - 48;
        if (DEBUG) {
          Serial.print("button=");
          Serial.print(numRead1);
          Serial.print(numRead2);
          Serial.println("");
        }
      }
    } else if (cmd2!=-1 && (byteRead==10 || byteRead==13)) {
      if (cmd2==1 && butRead1!=-1) {
        if (butRead2==-1) {
          butRead2=butRead1;
          butRead1=0;
        }
        int button = (butRead1*10) + butRead2 + 6;
        Serial.write("P");
        Serial.print(button, DEC);
        Serial.println("");
        pulseLed();
      }
      currentIndex = -1;
      butRead1 = -1;
      butRead2 = -1;
      cmd2 = -1;
    }
  }
}

void updateLevel(int inputLine) {
  if (DEBUG) {
    Serial.print("L");
    Serial.write( (65+inputLine) );
    Serial.print("=");
    Serial.print(levels[inputLine]);
    Serial.println("");
  }
  digitalPotWrite(cmd2pcb(inputLine), levels[inputLine]);
}

void updateLevels() {
  // go through the channels of the digital pot:
  for (int inputLine = 0; inputLine < 6; inputLine++) {
    if (DEBUG) {
      Serial.print("L");
      Serial.write( (65+inputLine) );
      Serial.print("=");
      Serial.print(levels[inputLine]);
      Serial.println("");
    }
    digitalPotWrite(cmd2pcb(inputLine), levels[inputLine]);
  }
}

void digitalPotWrite(int address, int value) {
  // take the SS pin low to select the chip:
  //byte byteA = (address << 6) & 0xFF;
  //byte byteB = value & 0xFF;
  //byte byte0 = byteA | byteB;
  //Serial.print(byte0, BIN);
  digitalWrite(slaveSelectPin, LOW);
  //SPI.transfer(byte0);
  SPI.transfer(address);
  SPI.transfer(value);
  digitalWrite(slaveSelectPin, HIGH);
}
