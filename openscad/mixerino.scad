pcbW = 77;
pcbH = 52;
pcbD = 2;

stW = 5;
stH = 10;
stD = 14;

topM = 2;

ffW = 2;
ffD = 6;

capD = 4;

walls = 2;
kerf = .1;


module screwTerminal(pins, holesOnly=false) {
  if (holesOnly) {
    union() {
      if (pins == 6) {
        color("Yellow", 1) {
          kerfX = .8; 
          translate([-kerfX, -4*walls, -kerf]) {
            cube([stW*pins+kerfX*2, stH+walls*8, stD+topM+kerf*2]);
          }
        }
      } else {
        color("Purple", 1) {
          translate([-kerf, -4*walls, -kerf]) {
            cube([stW*pins+kerf*2, stH+walls*8, stD+topM+kerf*2]);
          }
        }
      }
      color("Blue", 1) {
        for (i = [0:pins-1]) {
          $fn = 12;
          translate([stW/2+stW*i, stH/2, stD]) {
            cylinder(r=2.1, h=10, center=true);
          }
        }
      }
    }
  } else {
    color("LimeGreen", 1) {
      difference() {
        hull() {
          topD = 9;
          topSpace = 1;
          cube([stW*pins, stH, stD-topD]);
          translate([0, topSpace, stD-topD]) {
            cube([stW*pins, stH - (topSpace*2), topD]);
          }
        }
        color("Blue",1) {
          for (i = [0:pins-1]) {
            $fn = 12;
            translate([stW/2+stW*i, stH/2, stD]) {
              cylinder(r=1.9, h=2, center=true);
            }
          }
        }
      }
    }
  }
}

st1_ox = 2;
st1_oy = -2.5;
st1_2_d = 1.5;
st3_o = kerf*2;
st4_ox = 1.5;
st4_oy = 1;

module mixerinoBoardTerminals(holesOnly=false) {
  inputPins = 6;
  outputPins = 2;
  powerPins = 3;
  serialPins = 3;
  translate([st1_ox, st1_oy, 0]) {
    screwTerminal(inputPins, holesOnly);
    translate([stW*inputPins+st1_2_d, 0, 0]) {
      screwTerminal(inputPins, holesOnly);
    }
  }
  translate([pcbW, st3_o, 0]) {
    rotate([0, 0, 90]) {
      screwTerminal(outputPins, holesOnly);
      translate([pcbH-st3_o-stW*powerPins-st4_oy, 0, 0]) {
        translate([0, st4_ox, 0]) screwTerminal(powerPins, holesOnly);
        translate([0, pcbW-stH, 0]) {
          screwTerminal(serialPins, holesOnly);
        }
      }
    }
  }
}

module mixerinoBoard() {
  translate([0,0,-pcbD]) {
    cube([pcbW, pcbH, pcbD]);
  }
  mixerinoBoardTerminals();

}

module pcbFoot() {
  translate([0, 0, -pcbD-ffD-kerf]) {
    cube([ffW, ffW, ffD]);
  }
}

module pcbFeet() {
  translate([-kerf, -kerf, 0]) pcbFoot();
  translate([pcbW-ffW+kerf, -kerf, 0]) pcbFoot();
  translate([pcbW-ffW+kerf, pcbH-ffW+kerf, 0]) pcbFoot();
  translate([-kerf, pcbH-ffW+kerf, 0]) pcbFoot();
}

module pcbVolume() {
  color("Red", 1) {
    hull() {
      pcbFeet();
      translate([-kerf, -kerf, stD+topM+kerf]) {
        cube([pcbW+kerf*2, pcbH+kerf*2, kerf]);
      }
    }
  }
}

module bottomVolume() {
  color("Red", 1) {
    translate([-kerf-walls-kerf, -kerf-walls-kerf, 0]) {
      cube([pcbW+kerf*2+walls*2+kerf*2, pcbH+kerf*2+walls*2+kerf*2, stD+topM+kerf]);
    }
  }
}

module boxTop() {
  color("Yellow", .5) {
    difference() {
      translate([-kerf-walls*2, -kerf-walls*2, stD+topM+kerf-capD]) {
        cube([pcbW+kerf*2+walls*2+walls*2+kerf*2, pcbH+kerf*2+walls*2+walls*2+kerf*2, capD+walls]);
      }
      bottomVolume();
      mixerinoBoardTerminals(holesOnly=true);
    }
  }
}

module boxBottom() {
  color("LightGrey", 1) {
    union() {
      difference() {
        translate([-walls-kerf, -walls-kerf, -walls-pcbD-kerf-ffD]) {
          cube([pcbW+kerf*2+walls*2, pcbH+kerf*2+walls*2, walls+ffD+kerf+pcbD+stD+topM]);
        }
        pcbVolume();
        mixerinoBoardTerminals(holesOnly=true);
      }
      pcbFeet();
    }
  }
}

module box(top=false, bottom=false) {
  if (bottom) {
    boxBottom();
  }
  if (top) {
    rotate([0, 180, 0]) {
      translate([10, 0, -stD-topM-kerf]) {
        boxTop();
      }
    }    
  }
}

pcbVolume();
//mixerinoBoard();
//mixerinoBoardTerminals(holesOnly=true);
//bottomVolume();

//box(top=true, bottom=false);
//box(top=false, bottom=true);
