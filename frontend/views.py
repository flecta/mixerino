import sys 

from django.conf import settings
from django.shortcuts import redirect
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView

from backend.models import Seduta, Postazione


class HomeView(TemplateView):
    template_name = 'frontend/home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['sedute'] = Seduta.objects.all()
        return context

    def get(self, request, *args, **kwargs):
        seduta = Seduta.in_corso()
        print >>sys.stderr, 'seduta:', seduta
        if seduta is not None:
            return redirect('seduta', pk=seduta.pk)
        return super(HomeView, self).get(request, *args, **kwargs)
home = HomeView.as_view()


class SedutaView(DetailView):
    template_name = 'frontend/seduta.html'
    model = Seduta

    def get_context_data(self, **kwargs):
        context = super(SedutaView, self).get_context_data(**kwargs)
        context['levels'] = settings.ARDUINOWS
        context['postazioni'] = Postazione.objects.all()
        return context
seduta = SedutaView.as_view()
