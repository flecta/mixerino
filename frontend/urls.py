from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

from .views import home, seduta

urlpatterns = [
    url(r'^$', home),
    url(r'^seduta/(?P<pk>[0-9]+)$', seduta, name='seduta'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
